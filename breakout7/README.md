# Breakout 7 - Production setup

#Deploy a Kind cluster for demonstration purposes
#Deploy microK8s with Multipass and/or LXD
Sample deployment of machines on GCP
Deploy RKE2 on machines

Create 3 GCP instances
Install RKE2 as master on 1
Install RKE2 as agents on 2 & 3

gcloud compute instances create rke-cp rke-wk0 rke-wk1 --zone=us-central1-a --image-family ubuntu-2204-lts --image-project ubuntu-os-cloud --machine-type e2-standard-8
gcloud compute ssh --zone "us-central1-a" "rke-cp" --project "pewgk8training"
gcloud compute ssh --zone "us-central1-a" "rke-wk0" --project "pewgk8training"
gcloud compute ssh --zone "us-central1-a" "rke-wk1" --project "pewgk8training"

sudo
curl -sfL https://get.rke2.io | sh -


curl -sfL https://get.rke2.io | INSTALL_RKE2_TYPE="agent" sh -
/etc/rancher/rke2/config.yaml
```
server: https://<serverurl>:9345
token: <token from server>

