# Breakout session 4 - Configmaps and Secrets

The easiest way to set environment variables is directly in the pod template: 
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
spec:
  selector:
    matchLabels:
      app: guestbook
  replicas: 3
  template:
    metadata:
      labels:
        app: guestbook
        tier: frontend
    spec:
      containers:
      - name: frontend
        image: brightcomputing/guestbook-frontend:latest
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        env:
        - name: REDIS_SLAVE_REPLICAS
          value: "2"
        - name: REDIS_WRITE_TIMEOUT
          value: "10"
        ports:
        - containerPort: 8080
        readinessProbe:
          httpGet:
            path: /healthz
            port: 8080
          initialDelaySeconds: 5
          periodSeconds: 5
          timeoutSeconds: 3
```

The *env* section allows us to set environment variables that will be read in at pod creation time. Simple enough. What if we need to give our application a configuration file at startup time? We can't copy the file onto the container during runtime, as when we restart the pod, the copied file will be lost (container storage is ephemeral, remember). We could mount the file using a persistent volume (which we will talk about in a future lesson), but depending on the volume backend used, this can be unwieldy, and difficult to update easily. Enter the ConfigMap.

A ConfigMap can be used one of two ways. It can be used as the source of your environment variables, or it can be mounted as a file. Let's take a look at some examples. 

First let's set up a configmap. There's two ways to do this from the command line.

To set values directly as a key-value store (very useful for setting environment variables):

```kubectl create configmap example-cm --from-literal=TEST=value```

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: test
data:
  TEST: value
```

Additionally you can create a configmap directly from a file. This is useful for configuration files.

```kubectl create configmap example-file --from-file=config.conf=example.conf``` 

```
apiVersion: v1
data:
config.conf: |
  REDIS_SLAVE_REPLICAS="2"
  REDIS_WRITE_TIMEOUT="10"
kind: ConfigMap
metadata:
  name: example-cm 
```

Now, to use them in our Pod template. Let's take the environment variables out of the deployment file and put them into a configmap. 


```
kubectl create cm frontend-env --from-env-file=example.conf
```

The `from-env-file` flag will lead to the data in the file being loaded directly into the configmap object. This is very useful for setting a large number of environment variables at once. 

Replace the `env` section of the pod template in the frontend deployment with the following. An example of how to properly format this can be found at line 39 of the file `frontend-envfile.yaml`. 

```
envFrom:
  - configMapRef:
      name: frontend-env
```

This method is very useful if we have a large number of environment variables we want to set, and if we know we want to set all of the ones included in a given file. But what if we only want specific variables out of our file? For example, you may have one master configmap with all your variables set, and need to reference frontend-specific ones. Kubernetes allows you call out specific keys using the following format:

An example of this can be found at line 39 in `frontend-envkeys.yaml`. This setup can feel clunkier than setting the environment variables directly in the deployment file, but allows the variables to be source-controlled separately, which is often desirable. It also allows for the pattern of having one configmap provide variables to multiple related resources, while still allowing resources to use specific variables. 

```
env:
  - name: MY_ENV_VAR # By convention this should match the key name in the config map, but doesn't have to
    valueFrom:
      configMapKeyRef:
        name: frontend-env
        value: MY_ENV_KEY
```

The last common method for using configmaps is to mount them as files. This is particularly common for configuration files, or other smallish files that do not change during application run time. This is a good moment to mention some limitations of configmaps. First, config maps are read-only. This usually isn't a problem for configuration files, but it does mean that they can't be used for something like log files. 

Next, configmaps have a 1 MB file size limit. This again isn't usually an issue for configuration files, but if you want to use them to mount something like an image (think a web app icon), you need to be aware of this file size limitation. 

The final limitation is more of a limitation of Kubernetes itself than configmaps. If a configmap is mounted as a file, and it is changed, Kubernetes will pick up that change and the updated file will appear in the Pod. Kubernetes manages this by rechecking the configmap every minute to see if there are changes. If you have a large number of configmaps, this can add non-trivial load on your cluster. This isn't usually a problem until there are multiple thousands of configmaps on the cluster, but is something to be aware of. 

Here's an example of mounting a configmap as a file. 

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
  spec:
    containers:
      - name: mypod
          image: redis
        volumeMounts:
          - name: foo
            mountPath: "/etc/foo"
            readOnly: true
    volumes:
      - name: foo
        configMap:
          name: myconfigmap
```

### Secrets

One final limitation of configmaps is the lack of any confidentiality. This makes them a bad choice for storing secrets, such as passwords or api keys. The recommended method for storing this sort of data is to use a Kubernetes Secret object. 

Fundamentally a secret is very similar to a configmap. It can be used the same way, either as environment variables or mounted as a file, and it has the same size and write limitations. The major differences are in how they are stored. First, if you print out a secret, you will notice that it is not immediately readable.

Let's create a secret.

```
kubectl create secret generic example-secret --from-literal=username=myuser --from-literal=password=SuperS3cretP@ssw0rd
```

```
kubectl describe secret example-secret
```

```

Name:         example-secret
Namespace:    pewg
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
password:  19 bytes
username:  6 bytes
```

We need to dig a little bit to actually view the data stored in the secret. We'll use the json path format to take a look at our secret.

```
kubectl get secret example-secret -o jsonpath="{ .data }"

{"password":"U3VwZXJTM2NyZXRQQHNzdzByZA==","username":"bXl1c2Vy"}
```

Hmm, that's still not super useful. Keep in mind, that the values displayed when looking at this secret are not encrypted, they are encoded. Specifially, they are base64 encoded. Note, the values are encrypted in the etcd backend where they are actually stored, but not when printing them out. 

Let's see what it takes to actaully see our value:

```
kubectl get secret example-secret -o jsonpath="{ .data.password }" | base64 -d

SuperS3cretP@ssw0rd
```

There we go! This obviously won't prevent someone digging around from being able to access this password, but can prevent over-the-shoulder and accidental spills to a degree. Similarly, if you were to describe a pod that is using a secret, instead of printing the value, the pod will just indicate that the value is stored in a Secret object.

There are also additional types of secrets, which are listed [here](https://kubernetes.io/docs/concepts/configuration/secret/#secret-types). A couple of common ones worth mentioning are the "tls" secret, which simplify setting up TLS/SSL within your containers, and token/service-account-token, which hold secrets used by internal accounts.

At the end of the day, Secrets are useful, and certainly better for storing sensitive data than ConfigMaps, but for production situations it is usually worth looking at third party solutions, such as Hashicorp Vault, or cloud provided secrets management. 
