# Breakout session 5 - Storage

The ephemeral nature of containers presents a problem when running applications that require persistent storage. If a container is restarted for any reason (updates, crashes, etc), the underlying file system is reset to the image default. This is a problem for any stateful application, such as databases. This is where Volumes enter.

If you are familiar with Docker (or most other major container runtimes), the concept of a volume should be familiar. The runtime manages a space on the host disk that is mounted into the container namespace. This space is remounted when the container starts up, allowing data to be persisted across restarts.

Volumes in Kubernetes are very similar. Where they differ is how the volumes are created, and the kind of medium backs them. Kubernetes can handle this setup, depending on the volume type. A list of supported volume types can be found [here](https://kubernetes.io/docs/concepts/storage/volumes/).

We're going to explore persistence with our Redis pods. Open up the application in a browser, and add a few messages. Once you've done so, refresh the page (if you don't see the messages, add them again, then wait a few seconds before refreshing. This give the Redis replicas a chance to get in sync). 

Now, let's simulate a catastrophic failure. Kill the Redis pods, both the master and the slaves. You can do this by deleting the pods, or restarting the deployments. 

```
kubectl rollout restart deployment redis-master

kubectl rollout restart deployment redis-slave
```

Give the Redis pods a few moments to come back online, then refresh the application in the browser. 

Oh no! All of our messages are gone! This is due to the pods using the default, emphemeral storage. If we want to keep our data around, we're going to need a way to persist it. 

To begin with, we will need a StorageClass object. The StorageClass defines options related to the storage type, such as which (if any) provisioner to use, options that get applied at mount time, what happens to the volume on deletion, and more. Additional information on storage classes can be found [here](https://kubernetes.io/docs/concepts/storage/storage-classes/). A common use case for storage classes is to set up local, fast storage for applications that need rapid I/O as one class, and a separate network storage for archiving or other processes where latency is not as important. 

K3s comes with a default storage class preconfigured. We can check it out by running 

```
kubectl get storageclass

kubectl describe storageclass local-path
```

This storage class uses the "local-path" provisioner from Rancher, which is essentially a pod running in the kube-system namespace that will handle creating storage for us as we request it.

Let's add some persistence to our database. First, we will need a PersistentVolumeClaim. A volume claim is a request for a piece of the available storage on the cluster. At minimum, a claim needs to define the requested storage class, but it is common to also see access methods, reclaim policy, and storage requests in a claim definition. 

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: redis-pvc
  spec:
    accessModes:
      - ReadWriteOnce
    storageClassName: local-path
    resources:
      requests:
        storage: 128Mi
```

The example above can be found in redis-pvc.yaml. Go ahead and apply it now.

```
kubectl get pvc 

NAME        STATUS    VOLUME   CAPACITY   ACCESS MODES   STORAGECLASS   AGE
redis-pvc   Pending                                      local-path     32s
```

Our volume claim is created, it but nothing is currently using it, so it will remain in the Pending state. This is a behaviour of the storage-class. If you describe the class again, notice the "WaitForFirstConsumer" line in the description. Kubernetes won't actually attach the volume claim to a volume until a Pod requests the storage. So, let's attach our storage.

In redis-master.yaml, we're going to add two sections. First, at line 33, we'll connect our volume claim to this pod. This section will be at the same indent level as "containers" on line 17. 

```
volumes:
  - name: redis-data
    persistentVolumeClaim:
      claimName: redis-pvc
```

The name defined here is how we will reference this claim within the container definition, and claimName is the PVC we created earlier. 

Next, add the actual mount to the container definition. At line 28, at the same indent level as "image", add 

```
volumeMounts:
  - mountPath: "/data"
    name: redis-data
```

The full edits can be found in redis-master-storage.yaml if you get lost. 
Apply the changes. Once the master pod restarts, get our pvc list again.

```
kubectl get pvc

NAME        STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
redis-pvc   Bound    pvc-fcdc9e5a-1a2c-4f05-84e9-7dce11c6041b   128Mi      RWO            local-path     12m
```

Now that a pod is requesting storage, our volume claim gets bound to an actual volume. Go ahead and describe the pv listed under VOLUME (yours will be named differently)

```
Name:              pvc-fcdc9e5a-1a2c-4f05-84e9-7dce11c6041b
Labels:            <none>
Annotations:       pv.kubernetes.io/provisioned-by: rancher.io/local-path
Finalizers:        [kubernetes.io/pv-protection]
StorageClass:      local-path
Status:            Bound
Claim:             default/redis-pvc
Reclaim Policy:    Delete
Access Modes:      RWO
VolumeMode:        Filesystem
Capacity:          128Mi
Node Affinity:     
  Required Terms:  
    Term 0:        kubernetes.io/hostname in [ma-laptop]
Message:           
Source:
    Type:          HostPath (bare host directory volume)
    Path:          /var/lib/rancher/k3s/storage/pvc-fcdc9e5a-1a2c-4f05-84e9-7dce11c6041b_default_redis-pvc
    HostPathType:  DirectoryOrCreate
Events:            <none>
```

We can see that the default storage class provided with K3s creates "HostPath" type volumes. These volumes are great for development, and are very easy to understand if you are coming from Docker (they're kind of a blend of bind mounts and volumes), but they do not scale up well if you add additional nodes to your cluster. For more robust storage, you will likely want to use something cloud-based, like an awsElasticBlockStore or gcePersistentDisk type volume, or a self-hosted solution like GlusterFS or Longhorn.io. A list of volume types can be found [here](https://kubernetes.io/docs/concepts/storage/volumes/#volume-types).

Now, let's test our persistence. Go back to our application. There should be no messages. Go ahead and add a couple messages, then kill the redis pods again. Give them a few moments to restart, then go back to the application. You should still have messages. The data has persisted through a pod restart. Hooray!
