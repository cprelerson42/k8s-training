# Breakout session 2 - Deployments

Session objectives:
- Deploy redis backend deployments
- Edit deployments
- Scale deployments

Running individual pods is useful for quick tasks and troubleshooting purposes, but it doesn't take advantage of most of the features we're actually want to use from Kubernetes. Pods  by themselves don't scale or have any load-balancing or self-healing capabilities. For that, we need a ReplicaSet.

Instead of using ReplicaSets directly, we typically use one of the 3 replica set managers:

* Deployments
* StatefulSets
* DaemonSets

The most commonly used of these is the Deployment, so that is what we will focus on. Deployments provide a way of controlling a desired state for your ReplicaSet and Pod. We'll create a simple deployment file using the trick we used last time.

```kubectl create deployment redis --image redis --dry-run=client -o yaml > redis.yaml```

```
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: redis
  name: redis
spec:
  replicas: 1
  selector:
    matchLabels:
      app: redis
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: redis
    spec:
      containers:
      - image: redis
        name: redis
        resources: {}
status: {}
```

There's a couple of important lines to look at here. *Replicas* let us tell Kubernetes how many copies of the pod we desire. *Selector* allows us to tell the deployment which pods it's going to manage, usually based on labels. You do want to make sure that you use unique labels (or match more than one) so your deployment doesn't get attached to the wrong pods. 

*Strategy* allows us to control how pods are redeployed. The default strategy is "RollingUpdate", where Kubernetes will take pods down one at a time, and wait for there to be a given number of healthy pods available before taking the next one down. The other option is "Recreate", which tells Kubernetes to delete all the pods in a replica set and create new ones when there is an update. 

What is considered an update? Any change to the Pod template spec, which is what controls how the pods in a replica set are created. This section looks very similar to our standalone Pod deployment, and is where the image, environment, volume mounts, and other Pod specific configurations are set.

Deploy the manifest file (if you haven't already), then let's tweak a few things. Set replicas to 3, then reapply the file. 

```
kubectl get deployments

NAME    READY   UP-TO-DATE   AVAILABLE   AGE
redis   3/3     3            3           10m

kubectl get pods

NAME                     READY   STATUS    RESTARTS   AGE
redis-8464b6fbc9-r7679   1/1     Running   0          7m9s
redis-8464b6fbc9-wqt85   1/1     Running   0          11m
redis-8464b6fbc9-zfwkv   1/1     Running   0          7m9s
```

As you can see, the deployment has picked up the change, and deployed an additional two pods. Let's kill a pod, and see what happens. Take note of one of the pod's names (redis-8464b6fbc9-r7679 in the example given, yours will be different). Pods controlled by a deployment will be named [NAME OF DEPLOYMENT]-[HASH OF POD TEMPLATE]-[5 CHARACTER RANDOM STRING]. 
```
kubectl delete pod redis-8464b6fbc9-r7679

kubectl get pods

NAME                     READY   STATUS    RESTARTS   AGE
redis-8464b6fbc9-b74dl   1/1     Running   0          20s
redis-8464b6fbc9-wqt85   1/1     Running   0          15m
redis-8464b6fbc9-zfwkv   1/1     Running   0          10m
```

Notice that our Deployment has already rolled out a new pod for us, redis-8464b6fbc9-b74dl. Let's make another change and see what happens. Edit the image to be *redis:4.0* and reapply. Run ```kubectl get deployments``` and ```kubectl get pods``` and notice how the pods get terminated and recreated. This image redeploys pretty quickly, but you should see the pods go down one at a time and the new pods replace them. Try bumping up the replicas if necessary to see this happen. 

We can manually adjust the replicas like so:

```kubectl scale deployment redis --replicas 7```

Let's tweak the deployment file one more time, and change the image from *4.0* to *latest*, and change *strategy.type* to *Recreate*. Apply the file and watch the changes (note: I did a little cleanup on the file, removing some sections that get created when using the dry-run that aren't needed). 

```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: redis
  name: redis
spec:
  replicas: 4
  selector:
    matchLabels:
      app: redis
  strategy: 
    type: Recreate
  template:
    metadata:
      labels:
        app: redis
    spec:
      containers:
        - image: redis:latest
          name: redis
```

Actually, it's not very good practice to use the "latest" tag. Let's switch that to "7.0-alpine"

```kubectl set image deployment/redis redis=redis:7.0-alpine --record=true```

```
Flag --record has been deprecated, --record will be removed in the future
deployment.apps/redis image updated
```

Uh-oh, our frontend team just told us that something broke when we changed redis versions. We'd better roll back to 4.0. But when did we do that? Let's take a look at our revision history.

```kubectl rollout history deployment redis```

```
deployment.apps/redis 
REVISION  CHANGE-CAUSE
1         <none>
2         <none>
4         kubectl set image deployment/redis redis=redis:7.0-alpine --record=true
5         kubectl apply --filename=redis.yaml --record=true
```

Note, your history might look a little different, depending on whether or not you typo'ed the command while trying to write up instructions. We didn't turn on change recording until deployment 4, so we'll have to take a peek at the revision history to find when we last deployed Redis 4.0. 

```
kubectl rollout history deployment redis --revision 2
```

```
deployment.apps/redis with revision #2
Pod Template:
  Labels:	app=redis
	pod-template-hash=6756b4bcc5
  Containers:
   redis:
    Image:	redis:4.0
    Port:	<none>
    Host Port:	<none>
    Environment:	<none>
    Mounts:	<none>
  Volumes:	<none>
```

Aha! There we are, the pod template used in revision 2 used version 4.0. Let's rollback to that one to and let the devs test the app again.

```
kubectl rollout undo deployment redis --to-revision=2
```

```
deployment.apps/redis rolled back
```

```
kubectl describe deploy redis
```

```
Name:               redis
Namespace:          pewg
CreationTimestamp:  Mon, 10 Oct 2022 13:14:12 -0500
Labels:             app=redis
Annotations:        deployment.kubernetes.io/revision: 6
Selector:           app=redis
Replicas:           4 desired | 4 updated | 4 total | 4 available | 0 unavailable
StrategyType:       Recreate
MinReadySeconds:    0
Pod Template:
  Labels:  app=redis
  Containers:
   redis:
    Image:        redis:4.0
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
<output truncated>
```

There we are, we're back to 4.0. But we should really make sure that we update that in the deployment file, so that it can be source controlled. The rollout tool is very useful for testing changes, but we really want the source of truth to be our manifest file. Let's add an annotation in there as well to track our changes.

```
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    kubernetes.io/change-cause: "rollback to 4.0 due to app issues"
  labels:
    app: redis
  name: redis
spec:
  replicas: 4
  selector:
    matchLabels:
      app: redis
  strategy: 
    type: Recreate
  template:
    metadata:
      labels:
        app: redis
    spec:
      containers:
        - image: redis:4.0
          name: redis
```

Now when we view rollout history, we'll see our change reason as well. The use of annotation vs the *record* flag is a bit down to personal or organization preference. The record flag is deprecated, but exactly when that will be removed is still an open discussion on the Kubernetes github. There are also CI/CD tools that can handle changes like that for you. Plus, your manifests are all in source control, right? ;)

Go ahead and tweak any other values you feel like. In the next lession, we'll add a service to our deployments so we can actually reach the pods over the network.

