# Breakout session 3 - Services and Ingress

As of last session, we've got multiple redis pods running around, but none of them are actually capable of communicating with each other (or us for that matter). We also don't have anything making use of those pods. Let's start with that.

Go ahead and clean up the resources created last time if you haven't already. 

```kubectl delete deployment redis```

Go ahead and deploy our frontend application. 

```kubectl apply -f frontend.yaml```

Once it's up and running, we can verify it by hitting it with a local curl command, like we did in session 1.

```kubectl exec deploy/frontend -- curl localhost:8080```

(Notice the use of the shorthand *deploy/frontend* in place of our pod name. That's a handy shorthand that can be used in many commands)

Why did we hit port 8080? Take a look at the deployment file for our frontend.

```
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
spec:
  selector:
    matchLabels:
      app: guestbook
  replicas: 1
  template:
    metadata:
      labels:
        app: guestbook
        tier: frontend
    spec:
      containers:
      - name: frontend
        image: brightcomputing/guestbook-frontend:latest
        ports:
        - containerPort: 8080
```

On the last line of the container spec, we are calling out that we have something running on port 8080. Documenting that here does not actually do anything, but is a good practice for readablity purposes. Specifically, this helps us know that the application is running on port 8080.

So if declaring the port in the deployment file doesn't actually do anything, how do we get our container listening to the world? 

Kubernetes provides a resource called Services. A service is an abstract way of exposing a Pod's network to discovery. The primary motivation for services is the ephemeral nature of pods. Pods will be created and destroyed regularly as you update your application, change configurations, encounter crashes and outages, etc. Each time a pod is recreated, it (usually) winds up with a new IP address. 

The Service watches for pods based on the selectors provided in the service definition. When a new pod is created that matches the selector, the service takes care of updating the Endpoint resource, which track pod IP addresses. The service then updates Kubernetes' internal DNS records to reflect the updated endpoints. 

The end result of this is that the user (a human or application) never needs to know the Pod's actual IP address. All references to the pod will be done via the service name, which acts as a DNS record. So if our frontend service is named *frontend*, other pods can refer to it as *frontend* (or the fqdn *frontend.mynamespace.svc.cluster.local*). 

Let's take a look at a couple of the services that exist by default, then we'll try creating our own. 

```kubectl get service -A```

```
NAMESPACE     NAME             TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
default       kubernetes       ClusterIP      10.43.0.1       <none>        443/TCP                      50m
kube-system   kube-dns         ClusterIP      10.43.0.10      <none>        53/UDP,53/TCP,9153/TCP       50m
kube-system   metrics-server   ClusterIP      10.43.243.190   <none>        443/TCP                      50m
kube-system   traefik          LoadBalancer   10.43.134.198   10.128.0.6    80:30513/TCP,443:30100/TCP   50m
```

The *kubernetes* service in the *default* namespace will alway be created. This service allows pods to connect to the Kubernetes API, which is how any resource that creates or edits other resources does its job. The rest of the services will be slightly different depending on which flavor of Kubernetes you are running, but you will typically see some DNS server, which handles internal DNS, and commonly a metrics server. In our case we also have in ingress service in traefik, which we will talk about later. 

Let's create our own service. 

First we'll generate a minimal service config to look at.

```kubectl expose deployment frontend --dry-run=client -o yaml```

```
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  name: frontend
spec:
  ports:
  - port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app: guestbook
status:
  loadBalancer: {}
```

As we can see a service really boils down to two important bits - the ports and the selectors. By default, the port exposed was the one called out in the deployment manifest. A service can support multiple ports, each one defined in its own section. The port definition has three portions. The *port* defines what incoming port the service will listen on. The targetPort defines what internal port the traffic will be forwarded to. By default these are the same, but they don't have to be. Finally, the protocol tells the service what kind of traffic to accept. By default, it will be TCP, but any [of the supported](https://kubernetes.io/docs/concepts/services-networking/service/#protocol-support) protocols will work.

The selectors define what Pods the service will be attached too. Any valid label or combination of labels will work. By default, the *app* label is used.

Go ahead and apply our actual service file now.

```kubectl apply -f frontend-svc.yaml```

```
apiVersion: v1
kind: Service
metadata:
  name: frontend
  labels:
    app: guestbook
    tier: frontend
spec:
  ports:
  - port: 80
    targetPort: 8080
  selector:
    app: guestbook
    tier: frontend
```

```kubectl get svc```

```
NAME         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
frontend     ClusterIP   10.43.132.240   <none>        80/TCP         3s
```

Great! Now how do we reach our application? Well, we can't just yet. The Type of the Service we created is a ClusterIP. That means that it is assigned an IP on a network that is only accessible from inside the cluster. We could do this:

```kubectl run curl --image alpine/curl -- frontend```

```kubectl logs curl```

```
<html ng-app="redis">
<head>
<title>Guestbook</title>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.12/angular.min.js"></script>
<script src="app.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.0/ui-bootstrap-tpls.js"></script>
</head>
<body ng-controller="RedisCtrl">
<div style="width: 50%; margin-left: 20px">
<h2>Guestbook</h2>
<form>
<fieldset>
<input ng-model="msg" placeholder="Messages" class="form-control" type="text" name="input"><br>
<button type="button" class="btn btn-primary" ng-click="controller.onRedis()">Submit</button>
</fieldset>
</form>
<div>
<div ng-repeat="msg in messages track by $index">
{{msg}}
</div>
</div>
</div>
</body>
</html>
```

What did we just do? We started a pod using a very lightweight image that just runs curl, then gave it "frontend", our service name, as the hostname to grab. This returned the html from our application. Hooray! Still not really useful though. ClusterIP services are great for backend services, as they allow other applications to talk to them without allowing external traffic in. But how do we get our front end actually usable? 

There's three possible ways to handle this. Let's take a look at them. First, there's the NodePort service. This connects the service to a port on the Kubernetes node. This is the easiest way to get a reachable pod, as long as you are able to allow traffic through your firewall on the given port. If you have a cluster of machines, you will just need one of the control planes to have the port available, as the kube-proxy process will handle making sure the traffic goes where it needs to. When creating a NodePort, you can specify the port you want to use, or allow Kubernetes to assign it. In either case, it will be a port in the range 30000-32767.

### NodePorts

Let's change our service to be a NodePort.

```
apiVersion: v1
kind: Service
metadata:
  name: frontend
  labels:
    app: guestbook
    tier: frontend
spec:
  ports:
  - port: 80
    targetPort: 8080
    nodePort: 32000
  selector:
    app: guestbook
    tier: frontend
  type: NodePort
```

Now we can hit our application by using the IP address of our node, plus the port. 

```curl 35.222.153.16:32000```

If you use curl from your dev box, you can use the internal DNS name of the Kubernetes node.

```curl http://k3s-0:32000/```

If you want to hit the application from your browser, you'll need to use the external IP address of the node. 

There are two other main types of Services that we'll talk about, but will leave it up to the reader to work with. The LoadBalancer and the ExternalName services. 

The LoadBalancer service is used if you have an external service providing load balancing capability for you. In this case, you tell Kubernetes the IP of the load balancer, and it will handle traffic coming in from that IP and direct it per the service rules. If you are working with a cloud offering of Kubernetes, configuration can be set up such that creating a LoadBalancer type service will also create the actual LB on your cloud provider. Be aware of this, as this can incur additional costs with your cloud provider. If your cluster is not a cloud offering, you can still use the LB service alongside something like MetalLB, a load balancer designed specifically for Kubernetes on bare metal (or VMs). 

The final type of service, ExternalName, is mostly intended for hybridized workloads, where part of the application is running outside of the cluster. In the service definition, you provide the DNS name for the endpoint you are trying to hit, and as long as your nodes can resolve that hostname, this provides access to those endpoints from within the cluster. 

Before we move on to ingress, take a second and deploy the redis server, otherwise our app isn't going to work properly. 

```kubectl apply -f redis-master.yaml -f redis-slave.yaml```


### Ingress

Typically services are only intended for internal traffic. Allowing access to an endpoint using NodePorts can be a concern as you may need to open additional firewall ports. Services also can't provide any TLS/SSL capabilities or virtual hosting by name. For these capabilities, we need to use Ingress. We won't be going into everything that can be done with Ingress, but we'll get a simple one set up. 

In order to set up Ingress, you first need an ingress controller set up on your cluster. Our K3s comes pre-installed with Traefik, a popular choice for ingress controllers. You can read more about Traefik [here](https://doc.traefik.io/traefik/providers/kubernetes-ingress/). The ingress controller is simply a pod that will manage access to other services based on the rules set up in the ingress definition. 

Go ahead and create our ingress resource. 

```kubectl apply -f frontend-ingress.yaml```

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: frontendingress
spec:
  rules:
  - http:
      paths:
      - backend:
          service:
            name: frontend
            port:
              number: 80
```

Now you can navigate to the application at `http://<public_ip>/` using your browser. Go ahead and try it out. 


