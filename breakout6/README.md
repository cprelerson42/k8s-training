# Breakout session 6 - Helm

Helm is the de facto package manager for Kubernetes. It allows developers to easily version, share, and publish complex applications, and proivdes strong customization for application deployments. More information about it can be found [here](https://helm.sh).

Let's start by deploying a popular helm chart onto our cluster. Cert-Manager is a tool developed by Jetstack that makes issuing and renewing SSL certificates within your cluster much simpler. We're going to deploy is using Helm.

First, we need to add the Jetstack chart repo to our available repo list. 

```
helm repo add jetstack https://charts.jetstack.io

helm repo update

helm repo list
```

If Helm complains about your configuration file having incorrect permissions, you can correct them by running:

```
chmod 600 ~/.kube/config
```

Now let's see what charts (Helm's terminology for a deployable package) are available to us.

```
helm search repo jetstack

NAME                                    CHART VERSION   APP VERSION     DESCRIPTION
jetstack/cert-manager                   v1.10.0         v1.10.0         A Helm chart for cert-manager
(output truncated for readability)

```

There should be several charts available, but the one we care about it the first one, `jetstack/cert-manager`. We can see the chart version is 1.10.0, which deploys the matching version of the application. This is conventional, but not standardized or enforced, so if you need a specific application version it is important to check which chart version you need. 

Now that we are set up, let's install our chart.

```
helm upgrade --install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --set installCRDs=true --version v1.10.0
```

Let's break that down:

* `helm upgrade --install` - our command to install a chart. There is also a `helm install` command, but that will fail if the chart has already been installed. Popular convention is to use `upgrade --install` instead. 

* `cert-manager` - the name we are giving to our installation. Multiple copies of the same chart can be installed under different names, providing an immediately obvious use case for testing updates and having multiple environment on one cluster

* `jetstack/cert-manager` - the chart to install

* `--namespace cert-manager --create-namespace` - the namespace to install the chart into. If not specified it will go into default. The `--create-namespace` flag is necessary if the namespace does not already exist

* `--set installCRDs=true` - `--set` can be used to override default values for the chart on the command line. We'll talk more about this shortly. This specific value is one the Cert-Manager docs calls out as needing to be set (CRDs are Custom Resource Definitions, something we will not get to in this workshop, but more info can be found [here](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/). 

* `--version v1.10.0` - defining which version of the chart we want, based on the list we printed earlier. If omitted, the latest verison of the chart will be used. 


@TODO: Look at values, describe customization
Earlier we said that Helm provides a way to customize deployments. We saw that a little bit with the `--set` flag used in our install command. But that would be cumbersome to set a lot of variables, and can't be source controlled. Let's look at a more flexible option. Every Helm chart comes with a `values.yaml` file that contains predefined values for the variables that can be set. Often the chart's documentation will list commonly set ones, but it's also possible to just grab all of them. 

```
helm show values jetstack/cert-manager > values.yaml

cat values.yaml
```

This file lists all the possible variables that can be changed in a chart. They can be overridden at the command line using `--set`, or set directly in the file. A recommended way though is to create your own values.yaml, with only the values you are actaully changing. For example, to set the `installCRDs` value, your yaml file would look like this:

```
installCRDs: true
```

In order to use your own values file, it needs to be passed in with your install:

```
helm install -f values.yaml <other options as above>
```

This can also be used with the `upgrade` command. 


# Writing a Helm chart

We've seen how to use a community Helm chart, what about writing our own? There is a lot of flexibility and potential complexity in writing your own chart, but if you've got a good grasp of the Go Template syntax, we can get set one up pretty quickly. 

Start by running `helm create my-chart` to create a new chart, aptly named `my-chart`. This will give us the structure Helm expects to see in a chart. Navigate into the folder, then delete everything except the file `Chart.yaml`. (Feel free to peek through everything first, but the created example is way more complex than what need).

For this we're going to set up a simple Postgres database deployment. Create a new `templates` folder, and put the following into `templates/deploy.yaml`. This is about as simple of a Helm chart as we can have. 

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}-postgres
  labels:
    app: {{ .Chart.Name }}-postgres
spec:
  replicas: {{ .Values.replicas | default 1 }}
  selector:
    matchLabels:
      app: {{ .Chart.Name }}-postgres
  template:
    metadata:
      labels:
        app: {{ .Chart.Name }}-postgres
    spec:
      containers:
        - name: postgres
          image: postgres:{{ .Values.image.tag }}
          env:
            - name: POSTGRES_USER
              value: mario
            - name: POSTGRES_PASSWORD
              value: itsame
```

You will also need to create a `values.yaml` file in the same folder as `Chart.yaml`, and make it look like so:

```
image:
  tag: latest
```


This chart will deploy a Postgres database container, setting a default user and password. Deploy it by running `helm upgrade -i example-chart ~/k8s-training/breakout6/my-chart`. You shoudl get output similar to the following:

```
Release "example-chart" has been upgraded. Happy Helming!
NAME: example-chart
LAST DEPLOYED: Fri Nov  4 20:34:40 2022
NAMESPACE: default
STATUS: deployed
REVISION: 2
TEST SUITE: None
```

Feel free to interact with the created deployment and pod, at this point it behaves like any other Kubernetes resource. We used a couple of built in variables in our chart.

* {{ .Release.Name  }} will be the name you give your chart at installation time. `example-chart` in our case
* {{ .Chart.Name }} is the Chart name that we deployed. `my-chart` for our installation

We also declared our own variables, and indicated that they will come from the values file.

* {{ .Values.image.tag }} tells Helm that in the Values file, there will be an object called "image", and within that, an object called "tag"
* {{ .Values.replicas | default 1 }} tells Helm to look for the "replicas" object in the Values file, and if it's not declared, set the value to "1"

Take a moment to try and deploy using a different tag ([they can be found here](https://hub.docker.com/_/postgres/tags)) or change the number of replicas. You can redeploy using the `helm upgrade` command. 

Our database container works (you can verify by getting onto the pod via `exec` and running `psql` commands), but our username and password is hard-coded in our config. Let's do something about that. Start with creating `config.yaml` and `secrets.yaml` in the `my-charts/templates` folder, and adding the following: 

`configmap.yaml`

```
apiVersion: v1
kind: ConfigMap
metadata: 
  name: {{ .Release.Name }}-cm
data:
  POSTGRES_USER: {{ .Values.env.POSTGRES_USER }}
```

`secrets.yaml`

```
apiVersion: v1
kind: Secret
metadata:
  name: {{ .Release.Name }}-secret
data:
  POSTGRES_PASSWORD: {{ .Values.env.POSTGRES_PASSWORD | b64enc }}
```

And update `my-charts/template/deploy.yaml`

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}-postgres
  labels:
    app: {{ .Chart.Name }}-postgres 
spec:
  replicas: {{ .Values.replicas | default 1 }}
  selector:
    matchLabels:
      app: {{ .Chart.Name }}-postgres 
  template:
    metadata:
      labels:
        app: {{ .Chart.Name }}-postgres
    spec:
      containers:
        - name: postgres
          image: postgres:{{ .Values.image.tag }}
          env:
          {{ if .Values.useConfigObjects }}
            - name: POSTGRES_USER
              valueFrom:
                configMapKeyRef:
                  name: {{ .Release.Name }}-cm
                  key: POSTGRES_USER
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ .Release.Name}}-secret
                  key: POSTGRES_PASSWORD
          {{ else}}
            - name: POSTGRES_USER
              value: {{ .Values.env.POSTGRES_USER }}
            - name: POSTGRES_PASSWORD
              value: {{ .Values.env.POSTGRES_PASSWORD }}
          {{ end }}
```

There's several things to call out in our updates. First off, let's take a look at what we changed in the deployment. The Go Template syntax supports the `if-else-end` structure, which we are using to set our environment variables. If the `.Values.useConfigObjects` is true, the ConfigMap and Secret object we created will be used. If that value is false, the values will be set directly in the deployment file. (This example is a bit contrived to demonstrate the `if` structure). 

The last item we'll call out is the use of the `b64enc` function. Helm has access to a large set of functions provided by the Go template engine. The b64enc will base 64 encode the value from the file, a necessary step in creating a Secret. Other common available functions include boolean comparisons (and, not, gt, lt), string functions (contains, lower/upper, substr), math (add, max, min), and more. The full list can be found in the Helm documentation.

There is lots more to Helm, but that is all we are going to explore today. I'll leave it as an exercise for the reader to add persistence and a service that allows the database to be used remotely. 
